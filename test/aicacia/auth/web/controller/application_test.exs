defmodule Aicacia.Auth.Web.Controller.ApplicationTest do
  use Aicacia.Auth.Web.ConnCase

  alias Aicacia.Auth.Web.Guardian

  setup %{conn: conn} do
    user = get_user!()
    conn = Guardian.Plug.sign_in(conn, user.id)
    user_token = Guardian.Plug.current_token(conn)

    {:ok,
     user: user,
     conn:
       conn
       |> put_req_header("accept", "application/json")
       |> put_req_header(Aicacia.Auth.Service.User.authorization_header(), user_token)}
  end

  describe "create" do
    test "success", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.application_path(conn, :create), %{
          owner_id: user.id,
          name: "Test Application",
          home_url: "http://test.com",
          privacy_policy_url: "http://test.com/privacy_policy",
          terms_url: "http://test.com/terms",
          logo_url: "http://test.com/logo.png"
        })

      application_json = json_response(conn, 200)

      assert application_json ==
               %{
                 "id" => application_json["id"],
                 "owner_id" => user.id,
                 "name" => "Test Application",
                 "home_url" => application_json["home_url"],
                 "privacy_policy_url" => application_json["privacy_policy_url"],
                 "terms_url" => application_json["terms_url"],
                 "logo_url" => application_json["logo_url"],
                 "inserted_at" => application_json["inserted_at"],
                 "updated_at" => application_json["updated_at"]
               }
    end

    test "failure", %{conn: conn} do
      conn = post(conn, Routes.application_path(conn, :create), %{})

      application_json = json_response(conn, 422)

      assert application_json ==
               %{"errors" => %{"name" => ["can't be blank"]}}
    end
  end
end
