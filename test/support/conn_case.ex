defmodule Aicacia.Auth.Web.ConnCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use Phoenix.ConnTest
      alias Aicacia.Auth.Web.Router.Helpers, as: Routes

      @endpoint Aicacia.Auth.Web.Endpoint

      def get_user!() do
        Aicacia.Auth.Service.User.Repo.User.get_by_email!(System.get_env("EMAIL"))
      end
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Aicacia.Auth.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Aicacia.Auth.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
