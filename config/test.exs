use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :aicacia_auth, Aicacia.Auth.Web.Endpoint,
  http: [port: 4002],
  server: false,
  ui_url: "http://localhost:1234",
  api_url: "http://localhost:4000"

# Print only warnings and errors during test
config :logger, level: :warn

config :aicacia_auth, Aicacia.Auth.Mailer, adapter: Bamboo.TestAdapter

# Configure your database
config :aicacia_auth, Aicacia.Auth.Repo,
  database: "aicacia_auth_test",
  pool: Ecto.Adapters.SQL.Sandbox
