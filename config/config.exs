# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :aicacia_auth,
  ecto_repos: [Aicacia.Auth.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :aicacia_auth, Aicacia.Auth.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wF2JEGZt8htPRCCWxuDaB05lZDEhg8pIH72uX3LUsEW9JhnyBtOljer7JN4wyEdY",
  render_errors: [view: Aicacia.Auth.Web.View.Error, accepts: ~w(json)],
  pubsub: [name: Aicacia.Auth.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :aicacia_auth, Aicacia.Auth.Repo,
  username: "postgres",
  password: "postgres",
  hostname: System.get_env("DATABASE_HOST"),
  pool_size: 10,
  show_sensitive_data_on_connection_error: true

config :aicacia_auth, Aicacia.Auth.Web.Guardian,
  issuer: "aicacia_auth",
  secret_key: System.get_env("GUARDIAN_SECRET")

config :cors_plug,
  origin: ~r/.*/,
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"]

config :ueberauth, Ueberauth,
  providers: [
    facebook: {Ueberauth.Strategy.Facebook, []},
    github: {Ueberauth.Strategy.Github, [default_scope: "user:email"]},
    google: {Ueberauth.Strategy.Google, []},
    identity:
      {Ueberauth.Strategy.Identity,
       [
         callback_methods: ["POST"],
         uid_field: :email,
         nickname_field: :email
       ]},
    slack: {Ueberauth.Strategy.Slack, []},
    twitter: {Ueberauth.Strategy.Twitter, []}
  ]

config :ueberauth, Ueberauth.Strategy.Facebook.OAuth,
  client_id: System.get_env("FACEBOOK_APP_ID"),
  client_secret: System.get_env("FACEBOOK_APP_SECRET"),
  redirect_uri: System.get_env("FACEBOOK_REDIRECT_URI")

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: System.get_env("GITHUB_CLIENT_ID"),
  client_secret: System.get_env("GITHUB_CLIENT_SECRET")

config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  client_id: System.get_env("GOOGLE_CLIENT_ID"),
  client_secret: System.get_env("GOOGLE_CLIENT_SECRET"),
  redirect_uri: System.get_env("GOOGLE_REDIRECT_URI")

config :ueberauth, Ueberauth.Strategy.Slack.OAuth,
  client_id: System.get_env("SLACK_CLIENT_ID"),
  client_secret: System.get_env("SLACK_CLIENT_SECRET")

config :ueberauth, Ueberauth.Strategy.Twitter.OAuth,
  consumer_key: System.get_env("TWITTER_CONSUMER_KEY"),
  consumer_secret: System.get_env("TWITTER_CONSUMER_SECRET")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
