defmodule Aicacia.Auth.Service.User.SignInPassword do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.User.{SignInPassword, Repo}

  schema "" do
    field(:email, :string)
    field(:password, :string)
  end

  def changeset(%{} = params) do
    %SignInPassword{}
    |> cast(params, [:email, :password])
    |> validate_required([:email, :password])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      user_email = Repo.UserEmail.get_by!(email: command.email, confirmed: true)

      user_password = Repo.UserPassword.get_by!(user_id: user_email.user_id)

      if Argon2.check_pass(user_password.encrypted_password, command.password) do
        Repo.User.get!(user_email.user_id)
      else
        raise %Ecto.NoResultsError{}
      end
    end)
  end
end
