defmodule Aicacia.Auth.Service.User.AddPassword do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.User.{AddPassword, Repo}

  schema "" do
    field(:user_id, :binary_id)
    field(:password, :string)
    field(:password_confirmation, :string)
  end

  def changeset(%{} = params) do
    %AddPassword{}
    |> cast(params, [:user_id, :password, :password_confirmation])
    |> validate_required([:user_id, :password, :password_confirmation])
    |> validate_password_confirmation()
  end

  def validate_password_confirmation(%{changes: changes} = changeset) do
    password = changes.password

    case changes.password_confirmation do
      ^password -> changeset
      _ -> add_error(changeset, :password_confirmation, "invalid_password_confirmation")
    end
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      user = Repo.User.get!(command.user_id)

      %{password_hash: encrypted_password} = Argon2.add_hash(command.password)

      _user_password =
        Repo.UserPassword.create!(%{
          user_id: user.id,
          encrypted_password: encrypted_password
        })

      Repo.User.get!(user.id)
    end)
  end
end
