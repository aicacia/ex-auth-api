defmodule Aicacia.Auth.Service.User.Repo.UserEmail do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.UserEmail
  alias Aicacia.Auth.Service.User.Repo.Validation

  def get_by!(search), do: Repo.get_by!(UserEmail, search)

  def create!(%{} = attrs) do
    %UserEmail{}
    |> cast(attrs, [
      :user_id,
      :email
    ])
    |> validate_required([:user_id, :email])
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:email, name: :user_passwords_user_id_encrypted_password_index)
    |> Validation.put_confirmation_token()
    |> Repo.insert!()
  end

  def confirm_email!(%UserEmail{} = user_email) do
    user_email
    |> cast(%{primary: true, confirmed: true, confirmation_token: nil}, [
      :primary,
      :confirmed,
      :confirmation_token
    ])
    |> validate_required([:primary, :confirmed])
    |> Repo.update!()
  end
end
