defmodule Aicacia.Auth.Service.User.Repo.Provider do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.Provider

  def get_by_name!(name), do: Repo.get_by!(Provider, name: name)

  def create!(name) do
    %Provider{}
    |> cast(%{name: name}, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> Repo.insert!()
  end
end
