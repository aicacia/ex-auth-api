defmodule Aicacia.Auth.Service.User.Repo.ConfirmEmail do
  import Bamboo.Email

  def send_confirmation_email(email, confirmation_token) do
    confirmation_email(email, confirmation_token)
    |> Aicacia.Auth.Mailer.deliver_later()
  end

  defp confirmation_email(email, confirmation_token) do
    url = get_url(confirmation_token)

    new_email()
    |> from("info@aicacia.com")
    |> to(email)
    |> subject("Confirm Email")
    |> html_body(confirmation_email_html_body(url))
    |> text_body(confirmation_email_text_body(url))
  end

  defp get_url(confirmation_token),
    do:
      "#{Application.get_env(:aicacia_auth, Aicacia.Auth.Web.Endpoint)[:ui_url]}/confirm_email/#{
        confirmation_token
      }"

  defp confirmation_email_html_body(url),
    do: "<a target=\"_blank\" href=\"#{url}\">#{url}</a>"

  defp confirmation_email_text_body(url),
    do: url
end
