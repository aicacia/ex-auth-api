defmodule Aicacia.Auth.Service.User.Repo.UserPassword do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.UserPassword

  def get_by!(search), do: Repo.get_by!(UserPassword, search)

  def create!(%{} = attrs) do
    %UserPassword{}
    |> cast(attrs, [:user_id, :encrypted_password])
    |> validate_required([:user_id, :encrypted_password])
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:email, name: :user_passwords_user_id_encrypted_password_index)
    |> Repo.insert!()
  end
end
