defmodule Aicacia.Auth.Service.User.Repo.User do
  import Ecto.Query, warn: false

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.User

  def get(id), do: get_by_query(id) |> Repo.one()
  def get!(id), do: get_by_query(id) |> Repo.one!()

  defp get_by_query(id) do
    from(u in User,
      where: u.id == ^id,
      preload: [:applications, :emails, :password],
      select: u
    )
  end

  def get_by_email!(email) do
    user_email = Aicacia.Auth.Service.User.Repo.UserEmail.get_by!(email: email)
    get!(user_email.user_id)
  end

  def get_by_auth(nil), do: {:error, :not_found}

  def get_by_auth(%Ueberauth.Auth{info: nil} = _auth), do: get_by_auth(nil)

  def get_by_auth(%Ueberauth.Auth{info: info} = _auth) do
    case get_by_query(email: info.email) |> Repo.one() do
      nil -> get_by_auth(nil)
      user -> {:ok, user}
    end
  end

  def create!() do
    %User{}
    |> Repo.insert!()
  end
end
