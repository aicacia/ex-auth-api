defmodule Aicacia.Auth.Service.User.Repo.Validation do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  def put_confirmation_token(changeset),
    do: put_confirmation_token(changeset, Ecto.UUID.generate())

  def put_confirmation_token(changeset, uuid) do
    changeset_with_confirmation_token =
      changeset
      |> put_change(:confirmation_token, uuid)
      |> unique_constraint(:confirmation_token)

    if changeset_with_confirmation_token.valid?() do
      changeset_with_confirmation_token
    else
      put_confirmation_token(changeset)
    end
  end
end
