defmodule Aicacia.Auth.Service.User.ConfirmEmail do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.User.{ConfirmEmail, Repo}

  schema "" do
    field(:confirmation_token, :string)
  end

  def changeset(%{} = params) do
    %ConfirmEmail{}
    |> cast(params, [:confirmation_token])
    |> validate_required([:confirmation_token])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      user_email =
        Repo.UserEmail.get_by!(confirmation_token: command.confirmation_token)
        |> Repo.UserEmail.confirm_email!()

      Repo.User.get!(user_email.user_id)
    end)
  end
end
