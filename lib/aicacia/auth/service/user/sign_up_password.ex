defmodule Aicacia.Auth.Service.User.SignUpPassword do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.User.SignUpPassword
  alias Aicacia.Auth.Service.User.{Repo, AddEmail, AddPassword}

  schema "" do
    field(:email, :string)
    field(:password, :string)
    field(:password_confirmation, :string)
    field(:send_confirmation_email, :boolean)
  end

  def changeset(%{} = params) do
    %SignUpPassword{}
    |> cast(params, [:email, :password, :password_confirmation, :send_confirmation_email])
    |> validate_required([:email, :password, :password_confirmation, :send_confirmation_email])
    |> AddPassword.validate_password_confirmation()
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      user = Repo.User.create!()

      _user_email =
        AddEmail.new!(Map.merge(command, %{user_id: user.id}))
        |> AddEmail.handle!()

      _user_password =
        AddPassword.new!(Map.merge(command, %{user_id: user.id}))
        |> AddPassword.handle!()

      Repo.User.get!(user.id)
    end)
  end
end
