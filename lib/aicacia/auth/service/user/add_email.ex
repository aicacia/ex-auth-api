defmodule Aicacia.Auth.Service.User.AddEmail do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.User.{AddEmail, Repo}

  schema "" do
    field(:user_id, :binary_id)
    field(:email, :string)
    field(:send_confirmation_email, :boolean)
  end

  def changeset(%{} = params) do
    %AddEmail{}
    |> cast(params, [:user_id, :email, :send_confirmation_email])
    |> validate_required([:user_id, :email, :send_confirmation_email])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      user = Repo.User.get!(command.user_id)

      user_email =
        Repo.UserEmail.create!(%{
          user_id: user.id,
          email: command.email
        })

      if command.send_confirmation_email do
        Repo.ConfirmEmail.send_confirmation_email(
          user_email.email,
          user_email.confirmation_token
        )
      end

      Repo.User.get!(user.id)
    end)
  end
end
