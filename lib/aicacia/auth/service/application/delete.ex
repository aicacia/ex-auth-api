defmodule Aicacia.Auth.Service.Application.Delete do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.Application.Delete
  alias Aicacia.Auth.Service.Application.Repo

  schema "" do
    field(:owner_id, :binary_id)
    field(:application_id, :binary_id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:owner_id, :application_id])
    |> validate_required([:owner_id, :application_id])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      application =
        Repo.Application.get_by!(
          owner_id: command.owner_id,
          id: command.application_id
        )

      Repo.Application.delete!(application)
      application
    end)
  end
end
