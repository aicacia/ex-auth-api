defmodule Aicacia.Auth.Service.Application do
  def application_api_token_header(), do: "x-application-api-token"
end
