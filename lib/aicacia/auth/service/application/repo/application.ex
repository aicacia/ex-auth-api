defmodule Aicacia.Auth.Service.Application.Repo.Application do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.Application

  def get(id), do: Repo.get(Application, id)
  def get!(id), do: Repo.get!(Application, id)
  def get_by(search), do: Repo.get_by(Application, search)
  def get_by!(search), do: Repo.get_by!(Application, search)

  def all_query(),
    do:
      from(a in Application,
        preload: [:oauth2_credentials],
        select: a
      )

  def all(),
    do: all_query() |> Repo.all()

  def all_for_user_query(owner_id),
    do:
      from(a in Application,
        where: a.owner_id == ^owner_id,
        preload: [:oauth2_credentials],
        select: a
      )

  def all_for_user(owner_id),
    do: all_for_user_query(owner_id) |> Repo.all()

  def create!(%{} = attrs) do
    %Application{}
    |> cast(attrs, [:owner_id, :name, :home_url, :privacy_policy_url, :terms_url, :logo_url])
    |> validate_required([:owner_id, :name])
    |> foreign_key_constraint(:owner_id)
    |> unique_constraint(:name)
    |> Repo.insert!()
  end

  def update!(%Application{} = application, attrs \\ %{}) do
    application
    |> cast(attrs, [:owner_id, :name, :home_url, :privacy_policy_url, :terms_url, :logo_url])
    |> validate_required([:owner_id, :name])
    |> foreign_key_constraint(:owner_id)
    |> unique_constraint(:name)
    |> Repo.update!()
  end

  def delete!(%Application{} = application) do
    Repo.delete!(application)
  end
end
