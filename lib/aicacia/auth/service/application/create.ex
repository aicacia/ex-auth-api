defmodule Aicacia.Auth.Service.Application.Create do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.Application.Create
  alias Aicacia.Auth.Service.Application.Repo

  schema "" do
    field(:owner_id, :binary_id)
    field(:name, :string)
    field(:home_url, :string)
    field(:privacy_policy_url, :string)
    field(:terms_url, :string)
    field(:logo_url, :string)
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:owner_id, :name, :home_url, :privacy_policy_url, :terms_url, :logo_url])
    |> validate_required([:owner_id, :name])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      Repo.Application.create!(command)
    end)
  end
end
