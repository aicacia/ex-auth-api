defmodule Aicacia.Auth.Service.Application.Update do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.Application.{Update, Repo}

  schema "" do
    field(:application_id, :binary_id)
    field(:owner_id, :binary_id)
    field(:name, :string)
    field(:home_url, :string)
    field(:privacy_policy_url, :string)
    field(:terms_url, :string)
    field(:logo_url, :string)
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [
      :application_id,
      :owner_id,
      :name,
      :home_url,
      :privacy_policy_url,
      :terms_url,
      :logo_url
    ])
    |> validate_required([:owner_id, :application_id])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      Repo.Application.get_by!(
        owner_id: command.owner_id,
        id: command.application_id
      )
      |> Repo.Application.update!(command)
    end)
  end
end
