defmodule Aicacia.Auth.Service.OAuth2Credential.Repo.OAuth2Credential do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.OAuth2Credential
  alias Aicacia.Auth.Service.OAuth2Credential.Repo.Validation

  def get(id), do: Repo.get(OAuth2Credential, id) |> Repo.preload([:origins, :redirects])
  def get!(id), do: Repo.get!(OAuth2Credential, id) |> Repo.preload([:origins, :redirects])

  def get_by(search),
    do: Repo.get_by(OAuth2Credential, search) |> Repo.preload([:origins, :redirects])

  def get_by!(search),
    do: Repo.get_by!(OAuth2Credential, search) |> Repo.preload([:origins, :redirects])

  def all_for_application_query(application_id),
    do:
      from(oac in OAuth2Credential,
        where: oac.application_id == ^application_id,
        preload: [:origins, :redirects],
        select: oac
      )

  def all_for_application(application_id),
    do:
      all_for_application_query(application_id)
      |> Repo.all()

  def create!(%{} = attrs) do
    %OAuth2Credential{}
    |> cast(attrs, [:application_id, :name])
    |> validate_required([:application_id, :name])
    |> foreign_key_constraint(:application_id)
    |> Validation.put_unique_token(:client_id)
    |> Validation.put_unique_token(:client_secret)
    |> Repo.insert!()
  end

  def update!(%OAuth2Credential{} = oauth2_credential, attrs \\ %{}) do
    oauth2_credential
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> Repo.update!()
  end

  def update_client_secret_token!(%OAuth2Credential{} = oauth2_credential) do
    oauth2_credential
    |> cast(%{}, [])
    |> Validation.put_unique_token(:client_secret)
    |> unique_constraint(:api_token)
    |> Repo.update!()
  end

  def delete!(%OAuth2Credential{} = oauth2_credential) do
    Repo.delete!(oauth2_credential)
  end
end
