defmodule Aicacia.Auth.Service.OAuth2Credential.Repo.Validation do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  def validate_url(changeset, field, opts \\ []) do
    validate_change(changeset, field, fn _, value ->
      case URI.parse(value) do
        %URI{scheme: nil} ->
          "scheme_required"

        %URI{host: nil} ->
          "host_required"

        %URI{host: host} ->
          case :inet.gethostbyname(Kernel.to_charlist(host)) do
            {:ok, _} -> nil
            {:error, _} -> "invalid_host"
          end
      end
      |> case do
        error when is_binary(error) -> [{field, Keyword.get(opts, :message, error)}]
        _ -> []
      end
    end)
  end

  def put_unique_token(changeset, field),
    do: put_unique_token(changeset, field, Ecto.UUID.generate())

  def put_unique_token(changeset, field, uuid) do
    changeset_with_token =
      changeset
      |> put_change(field, uuid)
      |> unique_constraint(field)

    if changeset_with_token.valid?() do
      changeset_with_token
    else
      put_unique_token(changeset, field)
    end
  end
end
