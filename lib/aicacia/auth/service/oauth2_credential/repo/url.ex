defmodule Aicacia.Auth.Service.OAuth2Credential.Repo.URL do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.URL
  alias Aicacia.Auth.Service.OAuth2Credential.Repo.Validation

  def create!(%{} = attrs) do
    %URL{}
    |> cast(attrs, [:value])
    |> validate_required([:value])
    |> Validation.validate_url(:value)
    |> Repo.insert!()
  end

  def update!(%URL{} = url, attrs \\ %{}) do
    url
    |> cast(attrs, [:value])
    |> validate_required([:value])
    |> Validation.validate_url(:value)
    |> Repo.update!()
  end

  def delete!(%URL{} = url) do
    Repo.delete!(url)
  end
end
