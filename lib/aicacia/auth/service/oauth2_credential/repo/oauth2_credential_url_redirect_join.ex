defmodule Aicacia.Auth.Service.OAuth2Credential.Repo.OAuth2CredentialURLRedirectJoin do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.Auth.Repo
  alias Aicacia.Auth.Model.OAuth2CredentialURLRedirectJoin

  def create!(%{} = attrs) do
    %OAuth2CredentialURLRedirectJoin{}
    |> cast(attrs, [:oauth2_credential_id, :url_id])
    |> validate_required([:oauth2_credential_id, :url_id])
    |> unique_constraint(:oauth2_credential_id,
      name: :oauth2_credential_url_joins_oauth2_credential_id_url_id
    )
    |> Repo.insert!()
  end

  def delete!(%OAuth2CredentialURLRedirectJoin{} = oauth2_credential_url_join) do
    Repo.delete!(oauth2_credential_url_join)
  end
end
