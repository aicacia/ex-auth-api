defmodule Aicacia.Auth.Service.OAuth2Credential.Update do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.OAuth2Credential.{Update, Create, Repo}

  schema "" do
    field(:oauth2_credential_id, :id)
    field(:name, :string)
    field(:origins, {:array, :string})
    field(:redirects, {:array, :string})
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:oauth2_credential_id, :name, :origins, :redirects])
    |> validate_required([:oauth2_credential_id, :name])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      oauth2_credential = Repo.OAuth2Credential.get!(command)

      delete_urls(oauth2_credential.origins)
      delete_urls(oauth2_credential.redirects)

      Create.create_urls(
        oauth2_credential,
        command.origins,
        &Repo.OAuth2CredentialURLOriginJoin.create!/1
      )

      Create.create_urls(
        oauth2_credential,
        command.redirects,
        &Repo.OAuth2CredentialURLOriginJoin.create!/1
      )

      Repo.OAuth2Credential.get!(oauth2_credential.id)
    end)
  end

  defp delete_urls(urls),
    do:
      urls
      |> Enum.each(fn url ->
        Aicacia.Auth.Repo.delete!(url)
      end)
end
