defmodule Aicacia.Auth.Service.OAuth2Credential.Create do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.OAuth2Credential.{Create, Repo}

  schema "" do
    field(:application_id, :id)
    field(:name, :string)
    field(:origins, {:array, :string})
    field(:redirects, {:array, :string})
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:application_id, :name, :origins, :redirects])
    |> validate_required([:application_id, :name])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      oauth2_credential = Repo.OAuth2Credential.create!(command)

      create_urls(
        oauth2_credential,
        command.origins,
        &Repo.OAuth2CredentialURLOriginJoin.create!/1
      )

      create_urls(
        oauth2_credential,
        command.redirects,
        &Repo.OAuth2CredentialURLOriginJoin.create!/1
      )

      Repo.OAuth2Credential.get!(oauth2_credential.id)
    end)
  end

  def create_urls(oauth2_credential, urls, func),
    do:
      urls
      |> Enum.map(fn origin ->
        Repo.URL.create!(%{value: origin})
      end)
      |> Enum.each(fn url ->
        func.(%{
          oauth2_credential_id: oauth2_credential.id,
          url_id: url.id
        })
      end)
end
