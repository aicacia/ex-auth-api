defmodule Aicacia.Auth.Service.OAuth2Credential.Delete do
  use Aicacia.Handler

  alias Aicacia.Auth.Service.OAuth2Credential.{Delete, Repo}

  schema "" do
    field(:oauth2_credential_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:oauth2_credential_id])
    |> validate_required([:oauth2_credential_id])
  end

  def handle(%{} = command) do
    Aicacia.Auth.Repo.transaction(fn ->
      oauth2_credential = Repo.OAuth2Credential.get!(command.oauth2_credential_id)
      Aicacia.Auth.Repo.delete!(oauth2_credential)
      oauth2_credential
    end)
  end
end
