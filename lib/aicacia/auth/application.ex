defmodule Aicacia.Auth.Application do
  use Application

  def start(_type, _args) do
    children = [
      Aicacia.Auth.Repo,
      Aicacia.Auth.Web.Endpoint
    ]

    opts = [strategy: :one_for_one, name: Aicacia.Auth.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    Aicacia.Auth.Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
