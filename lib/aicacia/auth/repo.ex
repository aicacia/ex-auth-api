defmodule Aicacia.Auth.Repo do
  use Ecto.Repo,
    otp_app: :aicacia_auth,
    adapter: Ecto.Adapters.Postgres
end
