defmodule Aicacia.Auth.Model.OAuth2Credential do
  use Ecto.Schema

  alias Aicacia.Auth.Model.{
    Application,
    OAuth2CredentialURLOriginJoin,
    OAuth2CredentialURLRedirectJoin
  }

  schema "oauth2_credentials" do
    belongs_to(:application, Application)

    has_many(:origins_join, OAuth2CredentialURLOriginJoin, foreign_key: :oauth2_credential_id)
    has_many(:origins, through: [:origins_join, :url])

    has_many(:redirects_join, OAuth2CredentialURLRedirectJoin, foreign_key: :oauth2_credential_id)
    has_many(:redirects, through: [:redirects_join, :url])

    field(:name, :string)
    field(:client_id, :string)
    field(:client_secret, :string)

    timestamps(type: :utc_datetime)
  end
end
