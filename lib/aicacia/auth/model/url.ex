defmodule Aicacia.Auth.Model.URL do
  use Ecto.Schema

  schema "urls" do
    field(:value, :string)
    timestamps(type: :utc_datetime)
  end
end
