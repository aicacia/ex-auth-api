defmodule Aicacia.Auth.Model.OAuth2CredentialURLOriginJoin do
  use Ecto.Schema

  alias Aicacia.Auth.Model.{OAuth2Credential, URL}

  schema "oauth2_credential_url_origin_joins" do
    belongs_to(:oauth2_credential, OAuth2Credential)
    belongs_to(:url, URL)
  end
end
