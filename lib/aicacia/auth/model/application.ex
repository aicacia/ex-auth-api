defmodule Aicacia.Auth.Model.Application do
  use Ecto.Schema

  alias Aicacia.Auth.Model.{User, OAuth2Credential}

  schema "applications" do
    belongs_to(:owner, User, type: :binary_id)
    has_many(:oauth2_credentials, OAuth2Credential)

    field(:name, :string)
    field(:home_url, :string)
    field(:privacy_policy_url, :string)
    field(:terms_url, :string)
    field(:logo_url, :string)

    timestamps(type: :utc_datetime)
  end
end
