defmodule Aicacia.Auth.Model.UserPassword do
  use Ecto.Schema

  alias Aicacia.Auth.Model.User

  schema "user_passwords" do
    belongs_to(:user, User, type: :binary_id)

    field(:encrypted_password, :string)

    timestamps(type: :utc_datetime)
  end
end
