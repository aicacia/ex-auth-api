defmodule Aicacia.Auth.Model.UserEmail do
  use Ecto.Schema

  alias Aicacia.Auth.Model.{User}

  schema "user_emails" do
    belongs_to(:user, User, type: :binary_id)

    field(:email, :string)
    field(:primary, :boolean, default: false)
    field(:confirmed, :boolean, default: false)
    field(:confirmation_token, :string)

    timestamps(type: :utc_datetime)
  end
end
