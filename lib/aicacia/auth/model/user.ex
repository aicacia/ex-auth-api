defmodule Aicacia.Auth.Model.User do
  use Ecto.Schema

  alias Aicacia.Auth.Model.{
    Application,
    UserEmail,
    UserPassword
  }

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "users" do
    has_many(:applications, Application, foreign_key: :owner_id)
    has_many(:emails, UserEmail)
    has_one(:password, UserPassword)

    timestamps(type: :utc_datetime)
  end
end
