defmodule Aicacia.Auth.Model.Provider do
  use Ecto.Schema

  def password(), do: "password"

  schema "providers" do
    field(:name, :string)
  end
end
