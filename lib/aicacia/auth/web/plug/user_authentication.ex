defmodule Aicacia.Auth.Web.Plug.UserAuthentication do
  use Aicacia.Auth.Web, :plugs
  import Aicacia.Auth.Web.Plug.Helpers, only: [unauthorized: 1]

  alias Aicacia.Auth.Web.Guardian
  alias Aicacia.Auth.Model
  alias Aicacia.Auth.Service

  def init(opts), do: opts

  def call(conn, _opts) do
    authorize_connection(
      conn,
      get_req_header(conn, Service.User.authorization_header()) |> List.first()
    )
  end

  defp authorize_connection(conn, nil), do: unauthorized(conn)

  defp authorize_connection(conn, user_token) do
    case Guardian.decode_and_verify(user_token) do
      {:ok, %{"sub" => user_id}} ->
        assign_auth_data(conn, user_token, Service.User.Repo.User.get(user_id))

      _otherwise ->
        unauthorized(conn)
    end
  end

  defp assign_auth_data(conn, _token, nil), do: unauthorized(conn)

  defp assign_auth_data(conn, user_token, %Model.User{} = user) do
    conn
    |> assign(:user_token, user_token)
    |> assign(:user, user)
  end
end
