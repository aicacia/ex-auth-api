defmodule Aicacia.Auth.Web.Plug.ApplicationAuthentication do
  use Aicacia.Auth.Web, :plugs
  import Aicacia.Auth.Web.Plug.Helpers, only: [unauthorized: 1]

  alias Aicacia.Auth.Model
  alias Aicacia.Auth.Service

  def init(opts), do: opts

  def call(conn, opts) do
    authorize_connection(
      conn,
      conn.params[opts[:application_id_key]],
      conn.assigns[:user]
    )
  end

  defp authorize_connection(conn, _application_id, nil), do: unauthorized(conn)

  defp authorize_connection(conn, application_id, user) do
    assign_application_data(
      conn,
      Service.Application.Repo.Application.get_by(id: application_id, owner_id: user.id)
    )
  end

  defp assign_application_data(conn, nil), do: unauthorized(conn)

  defp assign_application_data(conn, %Model.Application{} = application) do
    conn
    |> assign(:application, application)
  end
end
