defmodule Aicacia.Auth.Web.Plug.Helpers do
  use Aicacia.Auth.Web, :plugs

  def unauthorized(conn) do
    conn
    |> put_status(401)
    |> render(Aicacia.Auth.Web.View.Error, "401.json")
    |> halt()
  end
end
