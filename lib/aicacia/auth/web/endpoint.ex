defmodule Aicacia.Auth.Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :aicacia_auth

  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  plug CORSPlug

  plug Aicacia.Auth.Web.Router
end
