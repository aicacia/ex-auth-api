defmodule Aicacia.Auth.Web.Router do
  use Aicacia.Auth.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :user_authenticated do
    plug(Aicacia.Auth.Web.Plug.UserAuthentication)
  end

  pipeline :application_authenticated do
    plug(Aicacia.Auth.Web.Plug.ApplicationAuthentication, application_id_key: "application_id")
  end

  if Mix.env() != :prod do
    forward "/sent-emails", Bamboo.SentEmailViewerPlug
  end

  scope "/", Aicacia.Auth.Web.Controller do
    pipe_through :api

    get("/oauth2/authorize", OAuth2, :authorize)

    scope "/auth", Auth do
      scope "/sign-up" do
        get("/:provider", SignUp, :request)
        get("/:provider/callback", SignUp, :callback)
        post("/:provider/callback", SignUp, :callback)
      end

      scope "/sign-in" do
        get("/:provider", SignIn, :request)
        get("/:provider/callback", SignIn, :callback)
        post("/:provider/callback", SignIn, :callback)
      end
    end

    scope "/user" do
      post("/confirm-email", User, :confirm_email)

      pipe_through :user_authenticated

      delete("/sign-out", User, :sign_out)
      get("/current-user", User, :current_user)
    end

    resources "/applications", Application, only: [:index, :show] do
      resources "/oauth2-credentials", OAuth2Credential, only: [:index, :show]
    end

    pipe_through(:user_authenticated)

    get("/applications-for-user", Application, :index_for_user)

    resources "/applications", Application, only: [:create, :update, :delete] do
      pipe_through(:application_authenticated)

      resources "/oauth2-credentials", OAuth2Credential, only: [:create, :update, :delete]
    end
  end
end
