defmodule Aicacia.Auth.Web.Guardian do
  use Guardian, otp_app: :aicacia_auth

  def subject_for_token(user_id, _claims) do
    {:ok, user_id}
  end

  def resource_from_claims(claims) do
    user_id = claims["sub"]
    resource = Aicacia.Auth.Service.User.Repo.User.get!(user_id)
    {:ok, resource}
  end
end
