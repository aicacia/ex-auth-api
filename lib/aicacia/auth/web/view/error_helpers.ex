defmodule Aicacia.Auth.Web.View.ErrorHelpers do
  def translate_error({msg, opts}) do
    if count = opts[:count] do
      Gettext.dngettext(Aicacia.Auth.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(Aicacia.Auth.Gettext, "errors", msg, opts)
    end
  end
end
