defmodule Aicacia.Auth.Web.View.UserEmail do
  use Aicacia.Auth.Web, :view
  alias Aicacia.Auth.Web.View.UserEmail

  def render("index.json", %{user_emails: user_emails}) do
    render_many(user_emails, UserEmail, "user_email.json")
  end

  def render("show.json", %{user_email: user_email}) do
    render_one(user_email, UserEmail, "user_email.json")
  end

  def render("user_email.json", %{user_email: user_email}) do
    %{
      id: user_email.id,
      email: user_email.email,
      confirmed: user_email.confirmed,
      primary: user_email.primary
    }
  end
end
