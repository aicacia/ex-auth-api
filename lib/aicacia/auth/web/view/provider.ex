defmodule Aicacia.Auth.Web.View.Provider do
  use Aicacia.Auth.Web, :view
  alias Aicacia.Auth.Web.View.Provider

  def render("index.json", %{providers: providers}) do
    render_many(providers, Provider, "provider.json")
  end

  def render("show.json", %{provider: provider}) do
    render_one(provider, Provider, "provider.json")
  end

  def render("provider.json", %{provider: provider}) do
    %{
      id: provider.id,
      name: provider.name
    }
  end

  def render("provider_name.json", %{provider: provider}) do
    provider.name
  end
end
