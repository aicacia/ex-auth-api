defmodule Aicacia.Auth.Web.View.OAuth2Credential do
  use Aicacia.Auth.Web, :view
  alias Aicacia.Auth.Web.View.OAuth2Credential

  def render("index.json", %{o_auth2_credentials: oauth2_credentials}) do
    render_many(oauth2_credentials, OAuth2Credential, "o_auth2_credential.json")
  end

  def render("show.json", %{o_auth2_credential: oauth2_credential}) do
    render_one(oauth2_credential, OAuth2Credential, "o_auth2_credential.json")
  end

  def render("o_auth2_credential.json", %{o_auth2_credential: oauth2_credential}) do
    %{
      id: oauth2_credential.id,
      application_id: oauth2_credential.application_id,
      name: oauth2_credential.name,
      client_id: oauth2_credential.client_id,
      client_secret: oauth2_credential.client_secret,
      origins: Enum.map(oauth2_credential.origins, fn origin -> origin.value end),
      redirects: Enum.map(oauth2_credential.redirects, fn redirect -> redirect.value end),
      inserted_at: oauth2_credential.inserted_at,
      updated_at: oauth2_credential.updated_at
    }
  end
end
