defmodule Aicacia.Auth.Web.View.Application do
  use Aicacia.Auth.Web, :view
  alias Aicacia.Auth.Web.View.Application

  def render("index.json", %{applications: applications}) do
    render_many(applications, Application, "application.json")
  end

  def render("show.json", %{application: application}) do
    render_one(application, Application, "application.json")
  end

  def render("application.json", %{application: application}) do
    %{
      id: application.id,
      owner_id: application.owner_id,
      name: application.name,
      home_url: application.home_url,
      privacy_policy_url: application.privacy_policy_url,
      terms_url: application.terms_url,
      logo_url: application.logo_url,
      inserted_at: application.inserted_at,
      updated_at: application.updated_at
    }
  end
end
