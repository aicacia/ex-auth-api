defmodule Aicacia.Auth.Web.Controller.Fallback do
  use Aicacia.Auth.Web, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(Aicacia.Auth.Web.View.Changeset)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(Aicacia.Auth.Web.View.Error)
    |> render(:"404")
  end
end
