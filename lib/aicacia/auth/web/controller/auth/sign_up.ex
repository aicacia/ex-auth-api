defmodule Aicacia.Auth.Web.Controller.Auth.SignUp do
  use Aicacia.Auth.Web, :controller

  alias Aicacia.Auth.Service.User.SignUpPassword

  plug(Ueberauth, base_path: "/auth/sign-up")

  action_fallback(Aicacia.Auth.Web.Controller.Fallback)

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_status(401)
    |> put_view(Aicacia.Auth.Web.View.Error)
    |> render("401.json")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    Aicacia.Auth.Web.Controller.User.sign_in_user(conn, sign_up(auth))
  end

  defp sign_up(%Ueberauth.Auth{provider: :identity, credentials: credentials} = auth) do
    case SignUpPassword.new(%{
           email: auth.info.email,
           password: credentials.other.password,
           password_confirmation: credentials.other.password_confirmation,
           send_confirmation_email: true
         }) do
      {:ok, command} ->
        SignUpPassword.handle(command)

      result ->
        result
    end
  end
end
