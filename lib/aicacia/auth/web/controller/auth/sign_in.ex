defmodule Aicacia.Auth.Web.Controller.Auth.SignIn do
  use Aicacia.Auth.Web, :controller

  alias Aicacia.Auth.Service.User.SignInPassword

  plug(Ueberauth, base_path: "/auth/sign-in")

  action_fallback(Aicacia.Auth.Web.Controller.Fallback)

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_status(401)
    |> put_view(Aicacia.Auth.Web.View.Error)
    |> render("401.json")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    Aicacia.Auth.Web.Controller.User.sign_in_user(conn, sign_in(auth))
  end

  defp sign_in(%Ueberauth.Auth{provider: :identity, credentials: credentials} = auth) do
    case SignInPassword.new(%{
           email: auth.info.email,
           password: credentials.other.password
         }) do
      {:ok, command} ->
        SignInPassword.handle(command)

      result ->
        result
    end
  end
end
