defmodule Aicacia.Auth.Web.Controller.OAuth2 do
  use Aicacia.Auth.Web, :controller

  alias Aicacia.Auth.Service

  action_fallback Aicacia.Auth.Web.Controller.Fallback

  @ui_url Application.get_env(:aicacia_auth, Aicacia.Auth.Web.Endpoint)[:ui_url]

  def authorize(
        conn,
        %{
          "client_id" => client_id
        }
      ) do
    oauth2_credential =
      Service.OAuth2Credential.Repo.OAuth2Credential.get_by!(client_id: client_id)

    conn
    |> redirect(
      "#{@ui_url}/oauth2/authorize?#{
        URI.encode_query(%{
          application_id: oauth2_credential.application_id,
          oauth2_credential_id: oauth2_credential.id
        })
      }"
    )
  end
end
