defmodule Aicacia.Auth.Web.Controller.OAuth2Credential do
  use Aicacia.Auth.Web, :controller

  alias Aicacia.Auth.Service.OAuth2Credential.{Create, Update, Delete}

  action_fallback Aicacia.Auth.Web.Controller.Fallback

  def index(conn, _params) do
    application = conn.assigns[:application]

    oauth2_credentials =
      Aicacia.Auth.Service.OAuth2Credential.Repo.OAuth2Credential.all_for_application(
        application.id
      )

    conn
    |> put_view(Aicacia.Auth.Web.View.OAuth2Credential)
    |> render("index.json", o_auth2_credentials: oauth2_credentials)
  end

  def show(conn, %{"id" => oauth2_credential_id}) do
    application = conn.assigns[:application]

    oauth2_credential =
      Aicacia.Auth.Service.OAuth2Credential.Repo.OAuth2Credential.get_by!(
        application_id: application.id,
        id: oauth2_credential_id
      )

    conn
    |> put_view(Aicacia.Auth.Web.View.OAuth2Credential)
    |> render("show.json", o_auth2_credential: oauth2_credential)
  end

  def create(conn, params) do
    application = conn.assigns[:application]

    with {:ok, command} <- Create.new(Map.merge(params, %{"application_id" => application.id})),
         {:ok, oauth2_credential} <- Create.handle(command) do
      conn
      |> put_view(Aicacia.Auth.Web.View.OAuth2Credential)
      |> render("show.json", o_auth2_credential: oauth2_credential)
    end
  end

  def update(conn, %{"id" => oauth2_credential_id} = params) do
    application = conn.assigns[:application]

    with {:ok, command} <-
           Update.new(
             Map.merge(params, %{
               "application_id" => application.id,
               "oauth2_credential_id" => oauth2_credential_id
             })
           ),
         {:ok, oauth2_credential} <- Update.handle(command) do
      conn
      |> put_view(Aicacia.Auth.Web.View.OAuth2Credential)
      |> render("show.json", o_auth2_credential: oauth2_credential)
    end
  end

  def delete(conn, %{"id" => oauth2_credential_id} = params) do
    application = conn.assigns[:application]

    with {:ok, command} <-
           Delete.new(
             Map.merge(params, %{
               "application_id" => application.id,
               "oauth2_credential_id" => oauth2_credential_id
             })
           ),
         {:ok, oauth2_credential} <- Delete.handle(command) do
      conn
      |> put_view(Aicacia.Auth.Web.View.OAuth2Credential)
      |> render("show.json", o_auth2_credential: oauth2_credential)
    end
  end
end
