defmodule Aicacia.Auth.Web.Controller.Application do
  use Aicacia.Auth.Web, :controller

  alias Aicacia.Auth.Service.Application.{Create, Update, Delete}

  action_fallback Aicacia.Auth.Web.Controller.Fallback

  def index(conn, _params) do
    applications = Aicacia.Auth.Service.Application.Repo.Application.all()

    conn
    |> put_view(Aicacia.Auth.Web.View.Application)
    |> render("index.json", applications: applications)
  end

  def index_for_user(conn, _params) do
    user = conn.assigns[:user]
    applications = Aicacia.Auth.Service.Application.Repo.Application.all_for_user(user.id)

    conn
    |> put_view(Aicacia.Auth.Web.View.Application)
    |> render("index.json", applications: applications)
  end

  def show(conn, %{"id" => application_id}) do
    application = Aicacia.Auth.Service.Application.Repo.Application.get!(application_id)

    conn
    |> put_view(Aicacia.Auth.Web.View.Application)
    |> render("show.json", application: application)
  end

  def create(conn, params) do
    user = conn.assigns[:user]

    with {:ok, command} <- Create.new(Map.merge(params, %{"owner_id" => user.id})),
         {:ok, application} <- Create.handle(command) do
      conn
      |> put_view(Aicacia.Auth.Web.View.Application)
      |> render("show.json", application: application)
    end
  end

  def update(conn, %{"id" => application_id} = params) do
    user = conn.assigns[:user]

    with {:ok, command} <-
           Update.new(
             Map.merge(params, %{"owner_id" => user.id, "application_id" => application_id})
           ),
         {:ok, application} <- Update.handle(command) do
      conn
      |> put_view(Aicacia.Auth.Web.View.Application)
      |> render("show.json", application: application)
    end
  end

  def delete(conn, %{"id" => application_id} = params) do
    user = conn.assigns[:user]

    with {:ok, command} <-
           Delete.new(
             Map.merge(params, %{"owner_id" => user.id, "application_id" => application_id})
           ),
         {:ok, application} <- Delete.handle(command) do
      conn
      |> put_view(Aicacia.Auth.Web.View.Application)
      |> render("show.json", application: application)
    end
  end
end
