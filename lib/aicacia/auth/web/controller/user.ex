defmodule Aicacia.Auth.Web.Controller.User do
  use Aicacia.Auth.Web, :controller

  alias Aicacia.Auth.Web.Guardian
  alias Aicacia.Auth.Model.User
  alias Aicacia.Auth.Service.User.ConfirmEmail

  action_fallback Aicacia.Auth.Web.Controller.Fallback

  def current_user(conn, _params) do
    user = conn.assigns[:user]
    user_token = conn.assigns[:user_token]

    conn
    |> put_view(Aicacia.Auth.Web.View.User)
    |> render("private_show.json", user: user, user_token: user_token)
  end

  def confirm_email(conn, %{"confirmation_token" => confirmation_token}) do
    case ConfirmEmail.new(%{
           confirmation_token: confirmation_token
         }) do
      {:ok, command} ->
        sign_in_user(conn, ConfirmEmail.handle(command))

      result ->
        result
    end
  end

  def sign_out(conn, _params) do
    user_token =
      conn
      |> get_req_header("authorization")
      |> List.first()

    with {:ok, _claims} <- Aicacia.Auth.Web.Guardian.revoke(user_token) do
      send_resp(conn, :no_content, "")
    end
  end

  def sign_in_user(conn, {:ok, %User{} = user}) do
    conn = Guardian.Plug.sign_in(conn, user.id)
    user_token = Guardian.Plug.current_token(conn)

    conn
    |> put_resp_header("authorization", user_token)
    |> put_view(Aicacia.Auth.Web.View.User)
    |> render("private_show.json", user: user, user_token: user_token)
  end

  def sign_in_user(conn, {:error, _reason}) do
    conn
    |> put_status(500)
    |> put_view(Aicacia.Auth.Web.View.Error)
    |> render("500.json")
  end
end
