defmodule Aicacia.Auth.Repo.Migrations.CreateApplications do
  use Ecto.Migration

  def change do
    create table(:applications) do
      add(:owner_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false)

      add(:name, :string, null: false)
      add(:home_url, :string)
      add(:privacy_policy_url, :string)
      add(:terms_url, :string)
      add(:logo_url, :string)

      timestamps(type: :utc_datetime)
    end

    create(index(:applications, [:owner_id]))
    create(unique_index(:applications, [:name]))
  end
end
