defmodule Aicacia.Auth.Repo.Migrations.CreateUserEmails do
  use Ecto.Migration

  def change do
    create table(:user_emails) do
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false

      add :primary, :boolean, null: false, default: false
      add :confirmed, :boolean, null: false, default: false
      add :confirmation_token, :string
      add :email, :string, null: false

      timestamps(type: :utc_datetime)
    end

    create(index(:user_emails, [:user_id]))
    create(unique_index(:user_emails, [:user_id, :email], name: :user_emails_user_id_email_index))
    create(unique_index(:user_emails, [:confirmation_token]))
  end
end
