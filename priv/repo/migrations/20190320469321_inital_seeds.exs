defmodule Aicacia.Auth.Repo.Migrations.InitialSeeds do
  use Ecto.Migration

  alias Aicacia.Auth.Service

  @ui_url Application.get_env(:aicacia_auth, Aicacia.Auth.Web.Endpoint)[:ui_url]
  @api_url Application.get_env(:aicacia_auth, Aicacia.Auth.Web.Endpoint)[:api_url]

  def up do
    Aicacia.Auth.Repo.transaction(fn ->
      _password_provider =
        Service.User.Repo.Provider.create!(Aicacia.Auth.Model.Provider.password())

      user_nathan =
        Service.User.SignUpPassword.new!(%{
          email: System.get_env("EMAIL"),
          password: System.get_env("PASSWORD"),
          password_confirmation: System.get_env("PASSWORD"),
          send_confirmation_email: false
        })
        |> Service.User.SignUpPassword.handle!()

      user_email = Service.User.Repo.UserEmail.get_by!(user_id: user_nathan.id)
      Service.User.Repo.UserEmail.confirm_email!(user_email)

      aicacia_auth_application =
        Service.Application.Create.new!(%{
          owner_id: user_nathan.id,
          name: "Aicacia Auth",
          home_url: @ui_url,
          privacy_policy_url: "#{@ui_url}/privacy-policy",
          terms_url: "#{@ui_url}/terms",
          logo_url: "#{@ui_url}/logo.png"
        })
        |> Service.Application.Create.handle!()

      _aicacia_auth_oauth2_credential =
        Service.OAuth2Credential.Create.new!(%{
          application_id: aicacia_auth_application.id,
          name: "Prod",
          origins: [@ui_url],
          redirects: ["#{@api_url}/oauth2/callback"]
        })
        |> Service.OAuth2Credential.Create.handle!()
    end)
  end
end
