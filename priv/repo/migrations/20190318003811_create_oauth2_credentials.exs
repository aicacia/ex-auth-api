defmodule Aicacia.Auth.Repo.Migrations.CreateOAuth2Credentials do
  use Ecto.Migration

  def change do
    create table(:oauth2_credentials) do
      add(:application_id, references(:applications, on_delete: :delete_all, type: :id),
        null: false
      )

      add(:name, :string, null: false)
      add(:client_id, :string, null: false)
      add(:client_secret, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:oauth2_credentials, [:application_id]))
    create(unique_index(:oauth2_credentials, [:application_id, :name]))
  end
end
