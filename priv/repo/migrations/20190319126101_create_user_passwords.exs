defmodule Aicacia.Auth.Repo.Migrations.CreateUserPasswords do
  use Ecto.Migration

  def change do
    create table(:user_passwords) do
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false

      add :encrypted_password, :string, null: false

      timestamps(type: :utc_datetime)
    end

    create(index(:user_passwords, [:user_id]))

    create(
      unique_index(:user_passwords, [:user_id, :encrypted_password],
        name: :user_passwords_user_id_encrypted_password_index
      )
    )
  end
end
