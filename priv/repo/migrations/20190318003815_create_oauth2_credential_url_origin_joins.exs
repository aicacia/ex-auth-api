defmodule Aicacia.Auth.Repo.Migrations.CreateOAuth2CredentialURLOriginJoins do
  use Ecto.Migration

  def change do
    create table(:oauth2_credential_url_origin_joins) do
      add(
        :oauth2_credential_id,
        references(:oauth2_credentials, on_delete: :delete_all, type: :id),
        null: false
      )

      add(:url_id, references(:urls, on_delete: :delete_all, type: :id), null: false)
    end

    create(index(:oauth2_credential_url_origin_joins, [:oauth2_credential_id]))
    create(index(:oauth2_credential_url_origin_joins, [:url_id]))
    create(unique_index(:oauth2_credential_url_origin_joins, [:oauth2_credential_id, :url_id]))
  end
end
