defmodule Aicacia.Auth.Repo.Migrations.CreateURLs do
  use Ecto.Migration

  def change do
    create table(:urls) do
      add :value, :string
      timestamps(type: :utc_datetime)
    end
  end
end
