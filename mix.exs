defmodule Aicacia.Auth.MixProject do
  use Mix.Project

  def name do
    :aicacia_auth
  end

  def version do
    "0.1.0"
  end

  def project do
    [
      app: name(),
      version: version(),
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Aicacia.Auth.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp env(:prod),
    do: [
      DOCKER_REGISTRY: "registry.aicacia.com",
      HELM_REPO: "https://chartmuseum.aicacia.com"
    ]

  defp env(_),
    do: [
      DOCKER_REGISTRY: "registry.localhost",
      HELM_REPO: "http://chartmuseum.localhost"
    ]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.0"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:poison, "~> 3.1"},
      {:cors_plug, "~> 2.0"},
      {:plug_cowboy, "~> 2.0"},
      {:scrivener_ecto, "~> 2.0"},
      {:aicacia_handler, "~> 0.1"},

      # Authentication
      {:argon2_elixir, "~> 2.0"},
      {:guardian, "~> 1.2"},
      {:ueberauth, "~> 0.6"},
      {:ueberauth_identity, "~> 0.2"},
      {:bamboo, "~> 1.3"},

      # Deployment
      {:distillery, "~> 2.0"}
    ]
  end

  defp namespace(), do: "api"
  defp helm_dir(), do: "./helm"
  defp get_env(key), do: Keyword.get(env(Mix.env()), key, "")

  defp docker_repository(), do: "#{get_env(:DOCKER_REGISTRY)}/api/#{name()}"
  defp docker_tag(), do: "#{docker_repository()}:#{version()}"

  defp helm_overrides(),
    do:
      "--set image.tag=#{version()} --set image.repository=#{docker_repository()} --set image.hash=$(mix docker.sha256) --set env.EMAIL='\"#{
        System.get_env("EMAIL")
      }\"' --set env.PASSWORD='\"#{System.get_env("PASSWORD")}\"' --set env.GUARDIAN_SECRET='\"#{
        System.get_env("GUARDIAN_SECRET")
      }\"'"

  defp createHelmInstall(values \\ nil),
    do:
      "helm install #{helm_dir()} --name #{name()} --namespace=#{namespace()} #{helm_overrides()} #{
        if values == nil, do: "", else: "--values #{values}"
      }"

  defp createHelmUpgrade(values \\ nil),
    do:
      "helm upgrade #{name()} #{helm_dir()} --namespace=#{namespace()} --install --force #{
        helm_overrides()
      } #{if values == nil, do: "", else: "--values #{values}"}"

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      postgres: [
        "cmd mkdir -p ${PWD}/.volumes/#{name()}-postgres",
        "cmd docker run --rm -d " <>
          "--name #{name()}-postgres " <>
          "-e POSTGRES_PASSWORD=postgres " <>
          "-p 5432:5432 " <>
          "-v ${PWD}/.volumes/#{name()}-postgres:/var/lib/postgresql/data " <>
          "postgres:11"
      ],
      "postgres.delete": [
        "cmd docker rm -f #{name()}-postgres",
        "cmd rm -rf ${PWD}/.volumes"
      ],
      test: ["ecto.create --quiet", "ecto.migrate", "test"],

      # Docker
      "docker.build": ["cmd docker build --build-arg MIX_ENV=#{Mix.env()} -t #{docker_tag()} ."],
      "docker.push": ["cmd docker push #{docker_tag()}"],
      "docker.sha256": [
        ~s(cmd docker inspect --format='"{{index .Id}}"' #{docker_tag()})
      ],

      # Helm
      "helm.push": [
        "cmd cd #{helm_dir()} && helm push . #{get_env(:HELM_REPO)} --username=\"#{
          get_env(:HELM_REPO_USERNAME)
        }\" --password=\"#{get_env(:HELM_REPO_PASSWORD)}\""
      ],
      "helm.install": ["cmd #{createHelmInstall()}"],
      "helm.install.local": ["cmd #{createHelmInstall("#{helm_dir()}/values-local.yaml")}"],
      "helm.upgrade": ["cmd #{createHelmUpgrade()}"],
      "helm.upgrade.local": ["cmd #{createHelmUpgrade("#{helm_dir()}/values-local.yaml")}"],
      helm: [
        "docker.build",
        "docker.push",
        "helm.push",
        "helm.upgrade"
      ],
      "helm.local": [
        "docker.build",
        "docker.push",
        "helm.push",
        "helm.upgrade.local"
      ]
    ]
  end
end
